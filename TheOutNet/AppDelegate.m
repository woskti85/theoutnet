//
//  Created by Tim Woska on 17/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//


#import "AppDelegate.h"

#import <RestKit/CoreData.h>
#import <RestKit/RestKit.h>
#import "RKXMLDictionarySerialization.h"

#import "TheOutNetProductService.h"
#import "HomeViewController.h"


NSString *const kBaseUrl = @"http://www.theoutnet.com";
NSString *const kProductKeyPath = @"ns2:products.product";

@interface AppDelegate ()

@property(nonatomic, strong) RKObjectManager *objectManager;
@property(nonatomic, strong) RKManagedObjectStore *managedObjectStore;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    //TODO: Need tests around setup of RestKit. Never used before so followed online tutorials   so code was not test driven.
    
    [self setUpRestKit];
    
    TheOutNetProductService *service = [[TheOutNetProductService alloc]initWithObjectManager:self.objectManager];
    
    UINavigationController *navigationController = (UINavigationController*) self.window.rootViewController;
    
    HomeViewController *homeViewController = (HomeViewController *) [navigationController topViewController];
    
    homeViewController.managedObjectContext = self.managedObjectStore.persistentStoreManagedObjectContext;
    homeViewController.service = service;
    
    return YES;
}

- (void)setUpRestKit {
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    [RKMIMETypeSerialization registerClass:[RKXMLDictionarySerialization class] forMIMEType:@"application/xml"];
    
    self.managedObjectStore = [self createManagedObjectStore];
    
    self.objectManager = [self createObjectManagerWithBaseUrlString:kBaseUrl mimeType:RKMIMETypeXML managedObjectStore:self.managedObjectStore responseDescriptor:[self productResponseDescriptor]];
    
    
}

- (RKObjectManager *)createObjectManagerWithBaseUrlString:(NSString *)baseUrlString mimeType:(NSString *)mimeType managedObjectStore:(RKManagedObjectStore *)managedObjectStore responseDescriptor:(RKResponseDescriptor *)responseDescriptor {
    
    NSURL *baseURL = [NSURL URLWithString:baseUrlString];
    
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseURL];
    
    objectManager.managedObjectStore = managedObjectStore;
    
    [self configureObjectManager:objectManager mimeType:mimeType];
    
    [objectManager addResponseDescriptor:responseDescriptor];
    
    return objectManager;
    
}

- (RKManagedObjectStore *)createManagedObjectStore {
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"TheOutNet.sqlite"];
    NSError *error;
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:@{NSInferMappingModelAutomaticallyOption : @YES, NSMigratePersistentStoresAutomaticallyOption :@YES} error:&error];
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    // Create the managed object contexts
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    return managedObjectStore;
}

- (void)configureObjectManager:(RKObjectManager *)objectManager mimeType:(NSString*)mimeType {
    
    [objectManager setAcceptHeaderWithMIMEType:mimeType];
    objectManager.requestSerializationMIMEType = mimeType;
    
}


- (RKResponseDescriptor *)productResponseDescriptor {
    
    RKEntityMapping *productMapping = [self productEntityMapping];
    
    RKResponseDescriptor *productResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:productMapping method:RKRequestMethodGET pathPattern:nil keyPath:kProductKeyPath statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    return productResponseDescriptor;
    
}

- (RKEntityMapping *)productEntityMapping {
    
    
    RKEntityMapping *productMapping = [RKEntityMapping mappingForEntityForName:@"Product" inManagedObjectStore:self.managedObjectStore];
    productMapping.identificationAttributes = @[@"id"];
    
    [productMapping addAttributeMappingsFromDictionary:@{@"_id":@"id",
                                                         @"_title":@"title",
                                                         @"_longDescription":@"longDescription",
                                                         @"_manufacturer":@"manufacturer",
                                                         @"_originalPrice":@"originalPrice",
                                                         @"_category":@"category"
                                                         }];
    
    return productMapping;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
}


@end
