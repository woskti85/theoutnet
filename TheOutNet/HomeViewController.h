//
//  Created by Tim Woska on 18/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TheOutNetProductService;

@interface HomeViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) TheOutNetProductService *service;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

@end
