//
//  Created by Tim Woska on 18/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import "HomeViewController.h"
#import "CategoryCollectionViewCell.h"
#import "CategoryListViewController.h"
#import "TheOutNetProductService.h"

@interface HomeViewController ()

@property (nonatomic,strong) NSArray *categoryArray;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"THE OUTNET";
    self.categoryArray = @[@"Clothing", @"Bags", @"Shoes", @"Accessories"];
}

#pragma mark - UICollectionView DatatSource methods

-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.categoryArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCollectionViewCell" forIndexPath:indexPath];
    
    NSString *category = [self.categoryArray objectAtIndex:indexPath.item];
    
    [self configureCell:cell category:category];
   
    return cell;
}

#pragma mark - CollectionViewDelegate methods

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CGFloat width = self.view.frame.size.width/2 - 30;
    CGFloat height = self.view.frame.size.height/2 -30;
    
    return CGSizeMake(width, height);
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.destinationViewController isKindOfClass:[CategoryListViewController class]]) {
        [self prepareForSegueToCategoryListViewController:segue.destinationViewController];
    }
}

- (void)prepareForSegueToCategoryListViewController:(CategoryListViewController *)viewController {
    
    [self.service requestForProductCategory:[self selectedCategory]];
    
    NSFetchedResultsController *fetchedResultsController = [self createFetchedResultsControllerForCategory:[self selectedCategory]];
    
    [self configureCategoryListViewController:viewController fetchedResultsController:fetchedResultsController];
    
}

#pragma mark - Helpers 

- (void)configureCell:(CategoryCollectionViewCell *)cell category:(NSString *)category {
    
    cell.label.text = category;
    cell.imageView.image = [UIImage imageNamed:category];
}

- (void)configureCategoryListViewController:(CategoryListViewController *)viewController fetchedResultsController:(NSFetchedResultsController *)fetchedResultsController {
    
    viewController.title = [[self selectedCategory] uppercaseString];
    
    viewController.fetchedResultsController = fetchedResultsController;
    
}

- (NSFetchedResultsController *)createFetchedResultsControllerForCategory:(NSString *)category {
    
    NSPredicate *predicate = [self createPredicateForCategory:category];
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
    
    NSFetchRequest *fetchRequest = [self createFetchRequestfetchWithpredicate:predicate sortDescriptor:sortDescriptor];
    
    return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
}

- (NSFetchRequest *)createFetchRequestfetchWithpredicate:(NSPredicate *)predicate sortDescriptor:(NSSortDescriptor *)sortDescriptor {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
    
    fetchRequest.predicate = predicate;
    fetchRequest.sortDescriptors = @[sortDescriptor];
    
    return fetchRequest;
    
}

- (NSPredicate *)createPredicateForCategory:(NSString *)category {
    
    return [NSPredicate predicateWithFormat:@"category == %@", category];
}

- (NSString*)selectedCategory {
    
    NSIndexPath *selectedIndexPath = [[self.collectionView indexPathsForSelectedItems] firstObject];
    
    NSString *selectedCategory = [self.categoryArray objectAtIndex:selectedIndexPath.item];
    
    return selectedCategory;
}

#pragma mark - orientation

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [self.collectionView.collectionViewLayout invalidateLayout];
}


@end
