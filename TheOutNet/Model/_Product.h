// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Product.h instead.

#import <CoreData/CoreData.h>

extern const struct ProductAttributes {
	__unsafe_unretained NSString *category;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *longDescription;
	__unsafe_unretained NSString *manufacturer;
	__unsafe_unretained NSString *originalPrice;
	__unsafe_unretained NSString *title;
} ProductAttributes;

@interface ProductID : NSManagedObjectID {}
@end

@interface _Product : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ProductID* objectID;

@property (nonatomic, strong) NSString* category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* id;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* longDescription;

//- (BOOL)validateLongDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* manufacturer;

//- (BOOL)validateManufacturer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* originalPrice;

//- (BOOL)validateOriginalPrice:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@end

@interface _Product (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCategory;
- (void)setPrimitiveCategory:(NSString*)value;

- (NSString*)primitiveId;
- (void)setPrimitiveId:(NSString*)value;

- (NSString*)primitiveLongDescription;
- (void)setPrimitiveLongDescription:(NSString*)value;

- (NSString*)primitiveManufacturer;
- (void)setPrimitiveManufacturer:(NSString*)value;

- (NSString*)primitiveOriginalPrice;
- (void)setPrimitiveOriginalPrice:(NSString*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

@end
