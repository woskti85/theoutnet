//
//  Created by Tim Woska on 18/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Product;

@interface ProductDetailViewController : UIViewController

@property(nonatomic,weak) IBOutlet UILabel *priceLabel;
@property(nonatomic,weak) IBOutlet UILabel *descriptionLabel;
@property(nonatomic,weak) IBOutlet UILabel *titleLabel;
@property(nonatomic,weak) IBOutlet UIImageView *imageView;

@property(nonatomic,strong) Product *product;

@end
