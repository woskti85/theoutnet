//
//  Created by Tim Woska on 18/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import "ProductDetailViewController.h"

#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "Product.h"
#import "UIImageView+AFNetworking.h"

@interface ProductDetailViewController ()

@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureView];

}

- (void)configureView {
    NSAssert(self.product, @"Must have a product injected");
    
    //TODO: Display and erorr if there is no product?
    
    self.priceLabel.text = self.product.originalPrice;
    self.descriptionLabel.text = self.product.longDescription;
    self.titleLabel.text = [NSString stringWithFormat:@"%@: %@", self.product.manufacturer, self.product.title];
    
    
    NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://cache.theoutnet.com/images/products/%@/%@_in_l.jpg",self.product.id, self.product.id]];
    [self.imageView setImageWithURL:imageUrl];
}



@end
