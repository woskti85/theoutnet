//
//  Created by Tim Woska on 17/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RKObjectManager;

@interface TheOutNetProductService : NSObject

- (instancetype)initWithObjectManager:(RKObjectManager *)objectManager;

- (void)requestForProductCategory:(NSString *)category;

@end
