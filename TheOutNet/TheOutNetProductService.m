//
//  Created by Tim Woska on 17/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import "TheOutNetProductService.h"
#import <RestKit/CoreData.h>
#import <RestKit/RestKit.h>
#import "Product.h"

NSString *const kPathFormatString = @"/Shop/%@/All?view=xml";

@interface TheOutNetProductService ()

@property (nonatomic,strong) RKObjectManager* objectManager;

@end

@implementation TheOutNetProductService


#pragma mark - Initialization 

- (instancetype)init {
    
    return [self initWithObjectManager:nil];
    
}

- (instancetype)initWithObjectManager:(RKObjectManager *)objectManager {
    
    NSAssert(objectManager, @"Object Manager must not be nil");
    
    self = [super init];
    if(self) {
        _objectManager = objectManager;
    }
    
    return self;
}

#pragma mark - Requests

- (NSMutableURLRequest *)createUrlRequestForCategory:(NSString *)category {
    NSString *path = [NSString stringWithFormat:kPathFormatString, category];
    NSMutableURLRequest *request = [self.objectManager requestWithObject:nil method:RKRequestMethodGET path:path parameters:nil];
    return request;
}

- (void)requestForProductCategory:(NSString *)category {
    
    NSMutableURLRequest *request = [self createUrlRequestForCategory:category];

    RKManagedObjectRequestOperation *operation = [self.objectManager managedObjectRequestOperationWithRequest:request managedObjectContext:self.objectManager.managedObjectStore.mainQueueManagedObjectContext success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        //TODO: should handle this with a dlegate call to notify the ViewController that the operation copmleted successfully
        
        NSLog(@"Loading mapping result: %lu", (unsigned long)result.count);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        //TODO: should handle this with a delegate call to notify the ViewController that the operation failed with an error.
        
        NSLog(@"Fail!");
    }];


    [operation start];

}



@end
