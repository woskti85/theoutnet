//
//  Created by Tim Woska on 18/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "CategoryListViewController.h"
#import "Product.h"
#import "ProductCollectionViewCell.h"
#import "ProductDetailViewController.h"



@interface CategoryListViewController ()

@property (nonatomic,strong) ProductDetailViewController *destinationViewController;

@end

@interface CategoryListViewControllerTests : XCTestCase

@property(nonatomic,strong) CategoryListViewController *viewController;

@property(nonatomic,strong) id mockFetchedResultsController;

@end

@implementation CategoryListViewControllerTests

- (void)setUp {
    [super setUp];
    
    self.viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CategoryListViewController"];
    
    self.mockFetchedResultsController = [OCMockObject niceMockForClass:[NSFetchedResultsController class]];
    
    self.viewController.fetchedResultsController = self.mockFetchedResultsController;
}

- (void)tearDown {
    
    
    self.viewController = nil;
    self.mockFetchedResultsController = nil;
    [super tearDown];
}

#pragma mark - Initialisation Tests

- (void)testViewController_CollectionViewDelegate {
    
    XCTAssertTrue([self.viewController conformsToProtocol:@protocol(UICollectionViewDelegate)]);
    
}

- (void)testViewController_CollectionViewDataSource {
    
    XCTAssertTrue([self.viewController conformsToProtocol:@protocol(UICollectionViewDataSource)]);
    
}

- (void)testViewController_FetchedResultsControllerDelegate {
    
    XCTAssertTrue([self.viewController conformsToProtocol:@protocol(NSFetchedResultsControllerDelegate)]);
    
}

- (void)testViewDidLoad_SetFetchedResultsControllerDelegateSelf {
    
    [(NSFetchedResultsController*)[self.mockFetchedResultsController expect] setDelegate:self.viewController];
    (void)self.viewController.view;
    
    XCTAssertNoThrow([self.mockFetchedResultsController verify]);
    
    
}

- (void)testViewDidLoad_FetchedResultsControllerPerformFetch {
    
    [[self.mockFetchedResultsController expect] performFetch:[OCMArg setTo:nil]] ;
    (void)self.viewController.view;
    
    XCTAssertNoThrow([self.mockFetchedResultsController verify]);
    
    
}

#pragma mark - UICollectionView Delegate Tests

- (void)testNumberOfSectionsInCollectionView_EqualsFectchedResultsControllerSections {
    
    NSArray *sections = @[@"",@"",@""];
    
    [[[self.mockFetchedResultsController stub] andReturn:sections] sections];
    
    NSInteger result = [self.viewController numberOfSectionsInCollectionView:nil];
    
    XCTAssertEqual(result,3);
    
}

- (void)testNumberOfItemsInSection {
    
    id mockSectionInfo = [OCMockObject mockForProtocol:@protocol(NSFetchedResultsSectionInfo)];
    
    [[[mockSectionInfo stub] andReturnValue:@99] numberOfObjects];
    
    NSArray *sections = @[mockSectionInfo];
    
    [[[self.mockFetchedResultsController stub] andReturn:sections] sections];
    
    
    NSInteger result = [self.viewController collectionView:nil numberOfItemsInSection:0];
    
    XCTAssertEqual(result, 99);
    
}

- (void)testCellForRowAtIndexPath {
    
    id mockSectionInfo = [OCMockObject mockForProtocol:@protocol(NSFetchedResultsSectionInfo)];
    
    [[[mockSectionInfo stub] andReturnValue:@99] numberOfObjects];
    
    NSArray *sections = @[mockSectionInfo];
    
    [[[self.mockFetchedResultsController stub] andReturn:sections] sections];
    
    (void)self.viewController.view;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    Product *product = [self configureTestProduct];
    
    [[[self.mockFetchedResultsController stub] andReturn:product ] objectAtIndexPath:indexPath];
    
    UICollectionViewCell *cell = [self.viewController collectionView:self.viewController.collectionView cellForItemAtIndexPath:indexPath];
    
    XCTAssertTrue([cell isKindOfClass:[ProductCollectionViewCell class]]);
    
}

- (void)testCellForRowAtIndexPath_CellTitleLabelEqualsProductTitle{
    
    id mockSectionInfo = [OCMockObject mockForProtocol:@protocol(NSFetchedResultsSectionInfo)];
    
    [[[mockSectionInfo stub] andReturnValue:@99] numberOfObjects];
    
    NSArray *sections = @[mockSectionInfo];
    
    [[[self.mockFetchedResultsController stub] andReturn:sections] sections];
    
    (void)self.viewController.view;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    Product *product = [self configureTestProduct];
    
    [[[self.mockFetchedResultsController stub] andReturn:product ] objectAtIndexPath:indexPath];
    
    ProductCollectionViewCell *cell = (ProductCollectionViewCell*)[self.viewController collectionView:self.viewController.collectionView cellForItemAtIndexPath:indexPath];
    
    XCTAssertEqualObjects(cell.titleLabel.text, product.title);
    
}

- (void)testCellForRowAtIndexPath_CellManufacturerLabelEqualsProductManufacturer{
    
    id mockSectionInfo = [OCMockObject mockForProtocol:@protocol(NSFetchedResultsSectionInfo)];
    
    [[[mockSectionInfo stub] andReturnValue:@99] numberOfObjects];
    
    NSArray *sections = @[mockSectionInfo];
    
    [[[self.mockFetchedResultsController stub] andReturn:sections] sections];
    
    (void)self.viewController.view;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    Product *product = [self configureTestProduct];
    
    [[[self.mockFetchedResultsController stub] andReturn:product ] objectAtIndexPath:indexPath];
    
    ProductCollectionViewCell *cell = (ProductCollectionViewCell*)[self.viewController collectionView:self.viewController.collectionView cellForItemAtIndexPath:indexPath];
    
    XCTAssertEqualObjects(cell.manufacturerLabel.text, product.manufacturer);
    
}

- (void)testCellForRowAtIndexPath_CellPriceLabelEqualsProductPrice{
    
    id mockSectionInfo = [OCMockObject mockForProtocol:@protocol(NSFetchedResultsSectionInfo)];
    
    [[[mockSectionInfo stub] andReturnValue:@99] numberOfObjects];
    
    NSArray *sections = @[mockSectionInfo];
    
    [[[self.mockFetchedResultsController stub] andReturn:sections] sections];
    
    (void)self.viewController.view;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    Product *product = [self configureTestProduct];
    
    [[[self.mockFetchedResultsController stub] andReturn:product ] objectAtIndexPath:indexPath];
    
    ProductCollectionViewCell *cell = (ProductCollectionViewCell*)[self.viewController collectionView:self.viewController.collectionView cellForItemAtIndexPath:indexPath];
    
    XCTAssertEqualObjects(cell.priceLabel.text, product.originalPrice);
    
}

- (void)testCellForRowAtIndexPath_CellImageViewImageNil {
    
    id mockSectionInfo = [OCMockObject mockForProtocol:@protocol(NSFetchedResultsSectionInfo)];
    
    [[[mockSectionInfo stub] andReturnValue:@99] numberOfObjects];
    
    NSArray *sections = @[mockSectionInfo];
    
    [[[self.mockFetchedResultsController stub] andReturn:sections] sections];
    
    (void)self.viewController.view;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    Product *product = [self configureTestProduct];
    
    [[[self.mockFetchedResultsController stub] andReturn:product ] objectAtIndexPath:indexPath];
    
    ProductCollectionViewCell *cell = (ProductCollectionViewCell*)[self.viewController collectionView:self.viewController.collectionView cellForItemAtIndexPath:indexPath];
    
    XCTAssertNil(cell.imageView.image);
    
}

#pragma mark - CollectionViewDelegate Tests

- (void)testDidSelectItemAtIndexPath_SetProductOnDestinationViewController {
    
    ProductDetailViewController *destinationViewController = [ProductDetailViewController new];
    
    self.viewController.destinationViewController = destinationViewController;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    
    Product *product = [self configureTestProduct];
    
    [[[self.mockFetchedResultsController stub] andReturn:product ] objectAtIndexPath:indexPath];
    
    [self.viewController collectionView:nil didSelectItemAtIndexPath:indexPath];
    
    XCTAssertEqual(self.viewController.destinationViewController.product, product);
    
}

#pragma mark - Navigation Tests

- (void)testPrepareForSegue {
    
    ProductDetailViewController *destinationViewController = [ProductDetailViewController new];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    [[[mockSegue stub] andReturn:destinationViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    XCTAssertEqual(self.viewController.destinationViewController, destinationViewController);
    
    
}

#pragma mark - NSFetchedResultsControllerDelegate Tests

// TODO: Add NSFetchedResultsControllerDelegateTests

#pragma mark - Helpers

-(NSManagedObjectContext *)testingManagedObjectContext {
    
    NSBundle *bundle = [NSBundle bundleForClass:[self.viewController class]];
    NSString* path = [bundle pathForResource:@"TheOutNet" ofType:@"momd"];
    NSURL *modURL = [NSURL URLWithString:path];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modURL];
    NSPersistentStoreCoordinator *coord = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: model];
    
    NSManagedObjectContext *testingContext = [[NSManagedObjectContext alloc] init];
    
    [testingContext setPersistentStoreCoordinator: coord];
    
    return testingContext;
    
}

-(Product *)configureTestProduct {
    
    Product *product = (Product *)[NSEntityDescription insertNewObjectForEntityForName:@"Product" inManagedObjectContext:[self testingManagedObjectContext]];
    
    product.originalPrice = @"testPrice";
    product.title = @"testTitle";
    product.category = @"Bags";
    product.longDescription = @"testDescription";
    product.manufacturer = @"testMaufacturer";
    
    return product;
}



@end
