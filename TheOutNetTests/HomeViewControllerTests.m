//
//  Created by Tim Woska on 18/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "HomeViewController.h"
#import "CategoryCollectionViewCell.h"
#import "CategoryListViewController.h"
#import "TheOutNetProductService.h"

@interface HomeViewController ()

@property (nonatomic,strong) NSArray *categoryArray;

@end

@interface HomeViewControllerTests : XCTestCase

@property(nonatomic,strong) HomeViewController *viewController;
@property(nonatomic,strong) CategoryListViewController *categoryListViewController;

@end

@implementation HomeViewControllerTests

- (void)setUp {
    
    [super setUp];
    
    self.viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeViewController"];
}

- (void)tearDown {
    
    self.viewController = nil;
    
    [super tearDown];
}

#pragma mark - Initialisation Tests

- (void)testViewController_CollectionViewDelegate {
    
    XCTAssertTrue([self.viewController conformsToProtocol:@protocol(UICollectionViewDelegate)]);
    
}

- (void)testViewController_CollectionViewDataSource {
    
    XCTAssertTrue([self.viewController conformsToProtocol:@protocol(UICollectionViewDataSource)]);
    
}

- (void)testViewDidLoad_SetsViewControllerTitle {
    
    
    (void)self.viewController.view;
    
    XCTAssertEqualObjects(self.viewController.title, @"THE OUTNET");
}

- (void)testViewDidLoad_SetsCategoryArray {
    
    
    (void)self.viewController.view;
    
    XCTAssertNotNil(self.viewController.categoryArray);
}

#pragma mark - UICollectionView DatatSource Tests 

- (void)testNumberOfSectionsInCollectionView {
    
    XCTAssertEqual([self.viewController numberOfSectionsInCollectionView:nil], 1);
}

- (void)testNumberOfItemsInSection_EqualsCategoryArrayCount {
    
    self.viewController.categoryArray = @[@"",@"",@""];
    
    XCTAssertEqual([self.viewController collectionView:nil numberOfItemsInSection:0], 3);
}

- (void)testCellForItemAtIndexPath_ReturnsCategoryCollectionViewCell {
    
    (void)self.viewController.view;
    
    UICollectionViewCell *returnedCell = [self.viewController collectionView:self.viewController.collectionView cellForItemAtIndexPath:nil];
    
    XCTAssertTrue([returnedCell isKindOfClass:[CategoryCollectionViewCell class]]);
}

- (void)testCellForItemAtIndexPath_CollectionViewCellLabelEqualsCorrespondingCategoryInCategoryArray {
    
    (void)self.viewController.view;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    
    self.viewController.categoryArray = @[@"Bags",@"Clothing"];
    
    CategoryCollectionViewCell *returnedCell = (CategoryCollectionViewCell *)[self.viewController collectionView:self.viewController.collectionView cellForItemAtIndexPath:indexPath];
    
    XCTAssertEqualObjects(returnedCell.label.text, @"Clothing");
    
    
}

- (void)testCellForItemAtIndexPath_CollectionViewCellIageViewSetsImage {
    
    (void)self.viewController.view;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    
    self.viewController.categoryArray = @[@"Bags",@"Clothing"];
    
    CategoryCollectionViewCell *returnedCell = (CategoryCollectionViewCell *)[self.viewController collectionView:self.viewController.collectionView cellForItemAtIndexPath:indexPath];
    
    XCTAssertNotNil(returnedCell.imageView.image);
    
}

#pragma mark - Navigation Tests 

- (void)testPrepareForSegue__RequestsCurrentlySelectedCategory {
    
    [self setUpPrepareForSegue];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    id mockService = [OCMockObject mockForClass:[TheOutNetProductService class]];
    
    [[mockService expect] requestForProductCategory:@"selectedItem"];
    
    self.viewController.service = mockService;
    self.viewController.managedObjectContext = [self testingManagedObjectContext];
    
    [[[mockSegue stub] andReturn:self.categoryListViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    XCTAssertNoThrow([mockService verify]);
}

- (void)testPrepareForSegue__CategoryListViewController_FetchedResultsControllerNotNil {
    
    [self setUpPrepareForSegue];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    self.viewController.managedObjectContext = [self testingManagedObjectContext];
    
    [[[mockSegue stub] andReturn:self.categoryListViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    XCTAssertNotNil(self.categoryListViewController.fetchedResultsController);
    
}

- (void)testPrepareForSegue__CategoryListViewController_FetchedResultsControllerManagedObjectContextEqualsViewControllerManagedObjectContext {
    
    [self setUpPrepareForSegue];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    self.viewController.managedObjectContext = [self testingManagedObjectContext];
    
    [[[mockSegue stub] andReturn:self.categoryListViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    XCTAssertEqual(self.categoryListViewController.fetchedResultsController.managedObjectContext, self.viewController.managedObjectContext);
    
}

- (void)testPrepareForSegue__CategoryListViewController_FetchedResultsControllerFetchRequestNotNil {
    
    [self setUpPrepareForSegue];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    self.viewController.managedObjectContext = [self testingManagedObjectContext];
    
    [[[mockSegue stub] andReturn:self.categoryListViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    XCTAssertNotNil(self.categoryListViewController.fetchedResultsController.fetchRequest);
    
}

- (void)testPrepareForSegue__CategoryListViewController_FetchedResultsControllerFetchRequestEntityNameEqualsProduct {
    
    [self setUpPrepareForSegue];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    self.viewController.managedObjectContext = [self testingManagedObjectContext];
    
    [[[mockSegue stub] andReturn:self.categoryListViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    XCTAssertEqualObjects(self.categoryListViewController.fetchedResultsController.fetchRequest.entityName, @"Product");
    
}

- (void)testPrepareForSegue__CategoryListViewController_FetchedResultsControllerFetchRequestPredicateNotNil {
    
    [self setUpPrepareForSegue];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    self.viewController.managedObjectContext = [self testingManagedObjectContext];
    
    [[[mockSegue stub] andReturn:self.categoryListViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    NSFetchRequest *fetchRequest = self.categoryListViewController.fetchedResultsController.fetchRequest;
    
    NSString *category = @"selectedItem";
    NSString *expectedPredicate = [NSString stringWithFormat:@"category == \"%@\"", category];
    
    NSString *resultPredicate = fetchRequest.predicate.predicateFormat;
    
     XCTAssertEqualObjects(resultPredicate, expectedPredicate);
    
}

- (void)testPrepareForSegue__CategoryListViewController_FetchedResultsControllerFetchRequestPredicateCorrect {
    
    [self setUpPrepareForSegue];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    self.viewController.managedObjectContext = [self testingManagedObjectContext];
    
    [[[mockSegue stub] andReturn:self.categoryListViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    NSFetchRequest *fetchRequest = self.categoryListViewController.fetchedResultsController.fetchRequest;
    
    NSString *category = @"selectedItem";
    NSString *expectedPredicate = [NSString stringWithFormat:@"category == \"%@\"", category];
    
    NSString *resultPredicate = fetchRequest.predicate.predicateFormat;
    
    XCTAssertEqualObjects(resultPredicate, expectedPredicate);
    
}

- (void)testPrepareForSegue__CategoryListViewController_TitleEqualsSelectedCategory {
    
    [self setUpPrepareForSegue];
    
    id mockSegue = [OCMockObject mockForClass:[UIStoryboardSegue class]];
    
    self.viewController.managedObjectContext = [self testingManagedObjectContext];
    
    [[[mockSegue stub] andReturn:self.categoryListViewController] destinationViewController];
    
    [self.viewController prepareForSegue:mockSegue sender:nil];
    
    XCTAssertEqualObjects(self.categoryListViewController.title, [@"SelectedItem" uppercaseString]);
    
}

#pragma mark - Helpers

- (NSManagedObjectContext *)testingManagedObjectContext {
    
    NSBundle *bundle = [NSBundle bundleForClass:[self.viewController class]];
    NSString* path = [bundle pathForResource:@"TheOutNet" ofType:@"momd"];
    NSURL *modURL = [NSURL URLWithString:path];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modURL];
    NSPersistentStoreCoordinator *coord = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: model];
    
    NSManagedObjectContext *testingContext = [[NSManagedObjectContext alloc] init];
    
    [testingContext setPersistentStoreCoordinator: coord];
    
    return testingContext;
    
}

- (void)setUpPrepareForSegue {
    
    (void)self.viewController.view;
    
    self.viewController.categoryArray =@[@"testItem", @"selectedItem"];
    
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForItem:1 inSection:0];
    
    [self.viewController.collectionView selectItemAtIndexPath:selectedIndexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    
    self.categoryListViewController = [CategoryListViewController new];
}

@end
