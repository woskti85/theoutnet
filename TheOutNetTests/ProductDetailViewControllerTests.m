//
//
//  Created by Tim Woska on 18/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ProductDetailViewController.h"
#import "Product.h"

@interface ProductDetailViewControllerTests : XCTestCase

@property (nonatomic,strong) ProductDetailViewController *viewController;

@end

@implementation ProductDetailViewControllerTests

- (void)setUp {
    [super setUp];
    
    self.viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
}

- (void)tearDown {
    
    self.viewController = nil;
    [super tearDown];
}

-(void)testViewDidLoad_NoProduct_Throws {
    
    XCTAssertThrows([self.viewController viewDidLoad]);
}

-(void)testViewDidLoad_Product_TitleLabelEqualsProductTitle {
    
    Product *product = [self configureTestProduct];
    
    self.viewController.product = product;
    
    (void)self.viewController.view;
    
    
    XCTAssertEqualObjects(product.title, self.viewController.titleLabel.text);
    
}

-(void)testViewDidLoad_Product_PriceLabelEqualsProductPrice {
    
    Product *product = [self configureTestProduct];
    
    self.viewController.product = product;
    
    (void)self.viewController.view;
    
    
    XCTAssertEqualObjects(product.originalPrice, self.viewController.priceLabel.text);

}

-(void)testViewDidLoad_Product_ManufacturerLabelEqualsProductManufacturer {
    
    Product *product = [self configureTestProduct];
    
    self.viewController.product = product;
    
    (void)self.viewController.view;
    
    
    XCTAssertEqualObjects(product.manufacturer, self.viewController.titleLabel.text);
    
}

-(void)testViewDidLoad_Product_DescriptionLabelEqualsProductDescription {
    
    Product *product = [self configureTestProduct];
    
    self.viewController.product = product;
    
    (void)self.viewController.view;
    
    
    XCTAssertEqualObjects(product.longDescription, self.viewController.descriptionLabel.text);
    
}

#pragma mark - Helpers 

-(NSManagedObjectContext *)testingManagedObjectContext {
    
    NSBundle *bundle = [NSBundle bundleForClass:[self.viewController class]];
    NSString* path = [bundle pathForResource:@"TheOutNet" ofType:@"momd"];
    NSURL *modURL = [NSURL URLWithString:path];
    NSManagedObjectModel *model = [[NSManagedObjectModel alloc] initWithContentsOfURL:modURL];
    NSPersistentStoreCoordinator *coord = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: model];
    
    NSManagedObjectContext *testingContext = [[NSManagedObjectContext alloc] init];
    
    [testingContext setPersistentStoreCoordinator: coord];
    
    return testingContext;
    
}

-(Product *)configureTestProduct {
    
    Product *product = (Product *)[NSEntityDescription insertNewObjectForEntityForName:@"Product" inManagedObjectContext:[self testingManagedObjectContext]];
    
    product.originalPrice = @"testPrice";
    product.title = @"testTitle";
    product.category = @"Bags";
    product.longDescription = @"testDescription";
    product.manufacturer = @"testMaufacturer";
    
    return product;
}



@end
