//
//  Created by Tim Woska on 17/01/2015.
//  Copyright (c) 2015 TheOutNet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <RestKit/CoreData.h>
#import <RestKit/RestKit.h>
#import <OCmock/Ocmock.h>
#import "TheOutNetProductService.h"


@interface TheOutNetProductService ()

@property (nonatomic, strong) RKObjectManager *objectManager;

@end

@interface TheOutNetProductServiceTests : XCTestCase

@property (nonatomic,strong) TheOutNetProductService *service;
@property (nonatomic,strong) id mockObjectManager;

@end

@implementation TheOutNetProductServiceTests

- (void)setUp {
    [super setUp];
    
    self.mockObjectManager = [OCMockObject niceMockForClass:[RKObjectManager class]];
    self.service = [[TheOutNetProductService alloc] initWithObjectManager:self.mockObjectManager];
    
}

- (void)tearDown {
    self.service = nil;
    [super tearDown];
}

#pragma mark - Initialisation

- (void)testInit_WithOutObjectManager_Throws {
    
    XCTAssertThrows([TheOutNetProductService new]);
}

- (void)testInit_WithObjectManager_ObjectManagerNotNil {

    XCTAssertNotNil(self.service.objectManager);
}

#pragma mark - Request

- (void)testRequstForProductCategory_Category_CreateUrlRequestForCategory {
    
    NSString *category = @"Category";
    
    NSString *expectedPath = [NSString stringWithFormat:@"/Shop/%@/All?view=xml",category];
    
    [[self.mockObjectManager expect] requestWithObject:nil method:RKRequestMethodGET path:expectedPath parameters:nil];
    
    [self.service requestForProductCategory:category];
    
    XCTAssertNoThrow([self.mockObjectManager verify]);
    
    
}

- (void)testRequstForProductCategory_CreateManagedObjectRquestOperation {
    
    [[self.mockObjectManager expect] managedObjectRequestOperationWithRequest:OCMOCK_ANY managedObjectContext:OCMOCK_ANY success:OCMOCK_ANY failure:OCMOCK_ANY];
    
    [self.service requestForProductCategory:nil];
    
    XCTAssertNoThrow([self.mockObjectManager verify]);
    
}

- (void)testRequstForProductCategory_ManagedObjectRequestOperationStart {
    
    id mangedObjectRequestOperation = [OCMockObject mockForClass:[RKObjectRequestOperation class]];
    
    [[[self.mockObjectManager stub] andReturn:mangedObjectRequestOperation] managedObjectRequestOperationWithRequest:OCMOCK_ANY managedObjectContext:OCMOCK_ANY success:OCMOCK_ANY failure:OCMOCK_ANY];
    
    [(RKObjectRequestOperation*)[mangedObjectRequestOperation expect] start];
    
    [self.service requestForProductCategory:nil];
    
    XCTAssertNoThrow([mangedObjectRequestOperation verify]);
    
}

-(void)testRequestForProductCategory_ManagedObjectRequestOperationSuccessful {
    
}

-(void)testRequestForProductCategory_ManagedObjectRequestOperationUnsuccessful {
    
}



@end
